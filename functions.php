<?php

/*
 * You can add your own functions here. You can also override functions that are
 * called from within the parent theme. For a complete list of function you can
 * override here, please see the docs:
 *
 * https://github.com/raamdev/independent-publisher#functions-you-can-override-in-a-child-theme
 *
 */


/*
 * Uncomment the following to add a favicon to your site. You need to add favicon
 * image to the images folder of Independent Publisher Child Theme for this to work.
 */
/*
function blog_favicon() {
  echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('stylesheet_directory').'/images/favicon.ico" />' . "\n";
}
add_action('wp_head', 'blog_favicon');
*/

/*
 * Add version number to main style.css file with version number that matches the
 * last modified time of the file. This helps when making frequent changes to the
 * CSS file as the browser will always load the newest version.
 */
/*
function independent_publisher_stylesheet() {
	wp_enqueue_style( 'independent-publisher-style', get_stylesheet_uri(), '', filemtime( get_stylesheet_directory() . '/style.css') );
}
*/

/*
 * Modifies the default theme footer.
 * This also applies the changes to JetPack's Infinite Scroll footer, if you're using that module.
 */
/*
function independent_publisher_footer_credits() {
	$my_custom_footer = 'This is my custom footer.';
	return $my_custom_footer;
}
*/

/* * * * * * 
 * Authors *
 * * * * * */

function independent_publisher_posted_author() {
	/**
	 * This function gets called outside the loop (in header.php),
	 * so we need to figure out the post author ID and Nice Name manually.
	 */
	global $wp_query;
	
	if ( function_exists( 'get_coauthors') ) {
	    $coauthors = get_coauthors( $wp_query->post->ID );
		
		printf('<span class="byline">');
		
		$i = 0;
		foreach($coauthors as $author) {
			$i++; // keep track of how many we've processed
			$post_author_id = $author->ID;
			$post_author_nice_name = get_the_author_meta( 'display_name', $post_author_id );

			printf(
		        '<span class="author p-author vcard h-card"><a class="u-url url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
		        esc_url( get_author_posts_url( get_the_author_meta( 'ID', $post_author_id ) ) ),
		        esc_attr( sprintf( __( 'View all posts by %s', 'independent-publisher' ), $post_author_nice_name ) ),
		        esc_html( $post_author_nice_name )
	        );

			if ( $i < (sizeof($coauthors) - 1) ) {
				// Add comma for all but last two authors.
				printf(', ');
			} else if ( $i == (sizeof($coauthors) - 1) ) {
				// Add "and" for second-last authors
				printf(' and ');
			}
			printf('Size: %$1d', sizeof($coauthors));
			// if last author, do nothing
		}
		printf('</span>');

	} else {
		$post_author_id        = $wp_query->post->post_author;
	    $post_author_nice_name = get_the_author_meta( 'display_name', $post_author_id );

		printf(
		    '<span class="byline"><span class="author p-author vcard h-card"><a class="u-url url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span></span>',
		    esc_url( get_author_posts_url( get_the_author_meta( 'ID', $post_author_id ) ) ),
		    esc_attr( sprintf( __( 'View all posts by %s', 'independent-publisher' ), $post_author_nice_name ) ),
		    esc_html( $post_author_nice_name )
	    );
	}
}

/* * * * * * * * *
 * Author cards  *
 * * * * * * * * */

function independent_publisher_posted_author_card() {
	/**
	 * This function gets called outside the loop (in header.php),
	 * so we need to figure out the post author ID and Nice Name manually.
	 */
	global $wp_query;
	$post_author_id = $wp_query->post->post_author;
	$show_avatars   = get_option( 'show_avatars' );
	?>

	<?php if ( ( !$show_avatars || $show_avatars === 0 ) && !independent_publisher_is_multi_author_mode() && get_header_image() ) : ?>
		<a class="site-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
			<img class="no-grav" src="<?php echo esc_url( get_header_image() ); ?>" height="<?php echo absint( get_custom_header()->height ); ?>" width="<?php echo absint( get_custom_header()->width ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
		</a>
	<?php else: ?>
		<a class="site-logo" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID', $post_author_id ) ); ?>">
			<?php echo get_avatar( get_the_author_meta( 'ID', $post_author_id ), 100 ); ?>
		</a>
	<?php endif; ?>

	<div class="site-title"><?php independent_publisher_posted_author(); ?></div>
	<div class="site-description"><?php the_author_meta( 'description', $post_author_id ) ?></div>

	<?php get_template_part( 'menu', 'social' ); ?>

	<div class="site-published-separator"></div>
	<h2 class="site-published"><?php _e( 'Published', 'independent-publisher' ); ?></h2>
	<h2 class="site-published-date"><?php independent_publisher_posted_on_date(); ?></h2>
	<?php /* Show last updated date if the post was modified AND
				Show Updated Date on Single Posts option is enabled AND
					'independent_publisher_hide_updated_date' Custom Field is not present on this post */ ?>
	<?php if ( get_the_modified_date() !== get_the_date() &&
		independent_publisher_show_updated_date_on_single() &&
		!get_post_meta( get_the_ID(), 'independent_publisher_hide_updated_date', true )
	) : ?>
		<h2 class="site-published"><?php _e( 'Updated', 'independent-publisher' ); ?></h2>
		<h2 class="site-published-date"><?php independent_publisher_post_updated_date(); ?></h2>
	<?php endif; ?>

	<?php do_action( 'independent_publisher_after_post_published_date' ); ?>
	<?php
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Setup theme                                                     *
 * Basically replicates the independent_publisher setup function,  *
 * but adds an extra topbar menu (and maybe more later)            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

function independent_publisher_setup() {
	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Customizer additions.
	 */
	require( get_template_directory() . '/inc/customizer.php' );

	/**
	 * Load support for Microformats 2
	 *
	 * @since 1.7.5
	 */
	require( get_template_directory() . '/inc/mf2.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 */
	load_theme_textdomain( 'independent-publisher', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable Custom Backgrounds
	 */
	add_theme_support(
		'custom-background', apply_filters(
			'independent_publisher_custom_background_args', array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Enable support for HTML5 markup.
	add_theme_support(
		'html5', array(
			'comment-list',
			'search-form',
			'comment-form',
			'gallery',
		)
	);

	/**
	 * Enable Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Add custom thumbnail size for use with featured images
	 */

	add_image_size( 'independent_publisher_post_thumbnail', 700, 700 );

	/**
	 * Enable editor style
	 */
	add_editor_style();

	/**
	 * Set max width of full screen visual editor to match content width
	 */
	set_user_setting( 'dfw_width', 700 );

	/**
	 * Set default value for Show Post Word Count theme option
	 */
	add_option( 'independent_publisher_general_options', array( 'show_post_word_count' => true ) );

	/**
	 * Set default value for Show Author Card theme option
	 */
	add_option( 'independent_publisher_general_options', array( 'show_author_card' => true ) );

	/**
	 * Set default value for Show Post Thumbnails theme option
	 */
	add_option( 'independent_publisher_excerpt_options', array( 'show_post_thumbnails' => false ) );

	/**
	 * This theme uses wp_nav_menu() in two locations.
	 */
	register_nav_menus(
		array(
			'primary' => __( 'Primary Menu', 'independent-publisher' ),
			'single'  => __( 'Single Posts Menu', 'independent-publisher' ),
			'social'  => __( 'Social', 'independent-publisher' ),
			'topbar'  => __( 'Topbar Menu', 'independent-snipette' ),
		)
	);

	/**
	 * Add support for the Aside Post Formats
	 */
	add_theme_support(
		'post-formats', array(
			'aside',
			'link',
			'gallery',
			'status',
			'quote',
			'chat',
			'image',
			'video',
			'audio'
		)
	);
}
